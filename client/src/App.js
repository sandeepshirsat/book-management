import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import './App.css';
import gql from 'graphql-tag';
// import { Query } from 'react-apollo';
import { Query, Mutation } from 'react-apollo';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import { createBrowserHistory } from "history";
const history = createBrowserHistory();

//select query for book table
const GET_BOOKS = gql`
{
  books {
    _id
    title
    author
    publisher
    published_year
  }
}
`;
//get book data as per id
const GET_BOOK = gql`
    query book($bookId: String) {
        book(id: $bookId) {
            _id
            isbn
            title
            author
            description
            published_year
            publisher
            updated_date
        }
    }
`;

//remove book data as per id
const DELETE_BOOK = gql`
  mutation removeBook($id: String!) {
    removeBook(id:$id) {
      _id
    }
  }
`;
//display book list
function App() {
    return (
      <>
      <Query pollInterval={500} query={GET_BOOKS}>
        {({ loading, error, data }) => {
          if (loading) return 'Loading...';
          if (error) return `Error! ${error.message}`;
          return (
            <div className="container">
              <div className="panel panel-default">
                <div className="panel-heading">
                  <h3 className="panel-title">
                    LIST OF BOOKS
                  </h3>
                  <h4><Link to="/create">Add Book</Link></h4>
                </div>
                <div className="panel-body">
                  <table className="table table-stripe">
                    <thead>
                      <tr>
                        <th>Title</th>
                        <th>Author</th>
                        <th>Publisher</th>
                        <th>Published year</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      {data.books.map((book, index) => (
                        <tr key={index}>
                          <td>{book.title}</td>
                          <td>{book.author}</td>
                          <td>{book.publisher}</td>
                          <td>{book.published_year}</td>
                          <td><Mutation mutation={DELETE_BOOK} key={book._id} onCompleted={() => history.push('/')}>
                                      {(removeBook, { loading, error }) => (
                                          <div>
                                              <form
                                                  onSubmit={e => {
                                                      e.preventDefault();
                                                      removeBook({ variables: { id: book._id } });
                                                  }}>
                                                  <Link to={`/edit/${book._id}`} className="btn btn-success">Edit</Link>&nbsp;
                                                  <button type="submit" className="btn btn-danger">Delete</button>
                                              </form>
                                          {loading && <p>Loading...</p>}
                                          {error && <p>Error :( Please try again</p>}
                                          </div>
                                      )}
                                  </Mutation></td>
                        </tr>
                      ))}
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          );
        }}
      </Query>
      </>
    );

}
export default App;