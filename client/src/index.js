import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import ApolloClient from 'apollo-boost';
import { ApolloProvider } from 'react-apollo';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import Edit from './components/Edit';
import Create from './components/Create';
import Show from './components/Show';

const client = new ApolloClient({
    uri: "http://159.89.161.95:3002/graphql" // or your graphql server uri //159.89.161.95
  });

  ReactDOM.render(
    <ApolloProvider client={client}>
        <Router>
         {/* <App /> */}
         <div>
            <Route exact path='/' component={App} />
            <Route path='/edit/:id' component={Edit} />
            <Route path='/create' component={Create} />
            <Route path='/show/:id' component={Show} />
        </div>
    </Router>
    </ApolloProvider>,
    document.getElementById('root')
);

serviceWorker.unregister();
